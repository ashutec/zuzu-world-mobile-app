import { Component, OnInit } from "@angular/core";
import { Page } from "ui/page";

@Component({
    selector: "ns-restaurant-list",
    moduleId: module.id,
    templateUrl: "./restaurant-list.component.html",
    styleUrls: ["restaurant-list.component.css"]
})
export class RestaurantListComponent {
    items: Array<any> = [
        { name: "Varburger", subTitle: "Burger,VEGAN" },
        { name: "Teahouse", subTitle: "TEA, COFFEE, BREAKFAST" },
        { name: "Italian Kitchen", subTitle: "ORGANIC, HEALTHY, FISH" }
    ];
    constructor(private page: Page) {
        this.page.actionBarHidden = true;
    }
}
