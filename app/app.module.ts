import { NgModule, NgModuleFactoryLoader, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";

import { registerElement } from "nativescript-angular";
import { Camera, Circle, MapView, Marker, Position, Style } from "nativescript-google-maps-sdk";
import * as platform from "platform";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { setStatusBarColors } from "./shared";

declare var GMSServices: any;

setStatusBarColors();

if (platform.isIOS) {
    GMSServices.provideAPIKey("AIzaSyArvUqW1Egulpf6aQoaiDXBbsX6OWpYxoI");
}
registerElement("CardView", () => require("nativescript-cardview").CardView);
registerElement("MapView", () => MapView);

@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        NativeScriptModule,
        AppRoutingModule
    ],
    declarations: [
        AppComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AppModule { }
