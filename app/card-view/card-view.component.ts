import { Component, OnInit } from "@angular/core";
import { Page } from "ui/page";

@Component({
    selector: "ns-card-view",
    moduleId: module.id,
    templateUrl: "./card-view.component.html",
    styleUrls: ["card-view.component.css"]
})
export class CardViewComponent {
    constructor(private page: Page) {
        this.page.actionBarHidden = true;
    }

}
